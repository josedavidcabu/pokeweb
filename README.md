<p align="center"><img src="img/titulo.png"></p>

_____

![image](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/25.gif)  Bienvenido a un aplicativo web con tematica de pokemon.

A continuación se explicará en lo que consiste el proyecto y sus tres principales partes.

![](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/143.gif) ![](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/385.gif) ![](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/196.gif) ![](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/182.gif) ![](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/628.gif) 


**Tabla de Contenido**

-[Información General](#Información-General)
* Nombre del proyecto
* Instalación
* Tecnologías
* IDE

-[Sección Home](#Sección-Home)
* [Descripción general](#Descripción-general)
* [Minijuego](#Minijuego)
* [PokeCatálogo](#PokeCatalogo)

-[Sección Trivia](#Sección-Trivia)

-[Sección Pokedex](#Sección-Pokedex)

<div id='Información-General'></div>

# Información General

## Nombre del proyecto
>PokeWeb.
## Instalación
La Api que utiliza este aplicativo web es la [PokeApi](https://pokeapi.co/docs/v2).
> Para hacer usó de manera correcta de este aplicativo web se debe usar un navegador Opera o Chrome.
## Tecnologías
> Las tecnologías que se usaron en este proyecto son: GIT, HTML, CSS, BOOTSTRAP, JAVASCRIPT.
## IDE
El IDE utilizado para este proyecto fue VisualStudio

## Autores
Jose David Castillo Buitrago - 1152021

Carlos Alberto Salazar Meza - 1152023

María Alexandra Velasquez J. - 1152026

<div id='Sección-Home'></div>

# Sección Home ![image](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/172.gif)

Esta sección es la inicial en donde se pueden acceder a las otras dos páginas. En primera instancia tiene la funcionalidad de darle al lector información general sobre pokémon constando de:

<div id='Descripción-general'></div>

## Descripción general
>En este apartado se presentan datos sobre en que consiste pokémon y una sección que da a conocer las diferentes **regiones** en las que se divide este mundo ficticio, presentadas mediante un carrusel con sus respectivas imagenes para una mejor visualización del lector.

<div id='Minijuego'></div>

## Minijuego
>Este apartado de esta sección es por donde se puede acceder al minijuego ¿Quien es ese pokémon?

<div id='PokeCatalogo'></div>

## PokeCatálogo
> El PokeCatalogo consiste en una referencia a la pokedex de pokémon, este es una enciclopedia virtual portátil de alta tecnología que los entrenadores Pokémon llevan consigo para registrar las fichas de todas las diversas especies Pokémon. 

La funcionalidad consiste en poder filtrar mediante **nombre**, **generación** y **tipo** de pokémon para facilitar su búsqueda. 

+ **Nombre o id**: Nombre o id del pokémon al cuál se desea consultar.
+ **Generación**: Muestra los pokémones que pertenecen a esta generación.
+ **Tipo**: Muestra los pokémones que pertenecen a este tipo.
    + **Generación y Tipo**:  Muestra los pokemones que pertenecen a la generación seleccionada y a su vez a el tipo seleccionado.

En este apartado se puede acceder a la sección pokedex.

<div id='Sección-Trivia'></div>

# Sección Trivia  ![image](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/4.gif)

Esta sección consta del conocido juego _**Who's that Pokemon?**_ o su versión en español _**¿Quién es ese Pokémon?**_ Al jugador se le da el nombre y la generación del pokémon, esté debe adivinar cuál es apartir de una serie de 4 imagenes; si acierta se le sumara un punto, pero si falla no anotara ninguno, pasando al siguiente nivel.

>## ¿TE CREES CON LA HABILIDAD DE ACEPTAR ESTE RETO?

<div id='Sección-Pokedex'></div>

# Sección Pokedex ![image](https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/40.gif)

En esta sección se puede acceder apartir de el **PokeCatálogo** (explicado anteriormente).
>Consta de una serie de datos que le dan al lector detalles sobre un pokémon.

Los datos que presenta son:
+ Vista general del pokémon
    + Una imagen de este
    + Su nombre con su respectivo id
    + Una Descripción
    + Tipo
+ Características
    + Estadísticas generales
    + Habilidades
    + Tipos efectivos
